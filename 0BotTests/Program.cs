﻿
using BotCore.Structures.Base;

using CoreBot;

using System;
using System.Diagnostics;
using System.Threading;

namespace _0BotTests
{
    class Program
    {
        static void Main()
        {
            Bot bot = new Bot("0BotTests");
            bot.Timing.PrintTimings = true;
            string timingName = "Tests";
            Stopwatch gsw = bot.Timing.StartTiming(timingName + ""), sw = bot.Timing.StartTiming((timingName += ".") + "Init");
            sw = bot.Timing.GotoNext(sw, timingName + "Login");
            bot.Start();
            sw = bot.Timing.GotoNext(sw, timingName + "TestBaseFunctions");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 seconds: {Util.ParseTime("10s")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 minutes: {Util.ParseTime("10m")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 hours: {Util.ParseTime("10h")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 days: {Util.ParseTime("10d")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 weeks: {Util.ParseTime("10w")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 months: {Util.ParseTime("10o")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 years: {Util.ParseTime("10y")}");
            Console.WriteLine($"[TEST] [Util.ParseTime] Testing time string parsing for 10 invalid: {Util.ParseTime("10i")}");
            Console.WriteLine($"[TEST] [Util.GetMemberCount] Grabbing total member count: {Util.GetUserCount(bot)}");
            Console.WriteLine($"[TEST] [Extensions.ContainsAnyOf] 'hello world, this is a test' contains 'hello' or 'oof': {"hello world, this is a test".ContainsAnyOf(new[] { "hello", "oof" })}");
            Console.WriteLine($"[TEST] [Extensions.ContainsAnyCase] 'Hello world, this is a test' contains 'hello': {"Hello world, this is a test".ContainsAnyCase("hello")}");
            Console.WriteLine($"[TEST] [Extensions.ApplyVars] 'BotCore $VER serving $USERS users': {"BotCore $VER serving $USERS users".ApplyVars(bot)}");
            Console.WriteLine($"[TEST] [Extensions.SaveToJsonFile] Saving BotInit object"); bot.SaveToJsonFile("test.json");
            Console.WriteLine($"[TEST] [Extensions.CountInstancesOfAll] 'hello world' amount of L and O (5): {"hello world".CountInstancesOfAll(new[] { "l", "o" })}");
            bool test = true; Console.WriteLine($"[TEST] [Extensions.Toggle] Togging bool from {test}: {test.Toggle()} {test}");

            sw = bot.Timing.GotoNext(sw, timingName + "Logoff");
            //bot.Stop();

            bot.Timing.StopTiming(sw);
            bot.Timing.StopTiming(gsw);
            Thread.Sleep(100000);
        }
    }
}
