﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using BotCore.Structures.Base;
using BotCore.Structures.ServiceWorkers;
using CoreBot;
using CoreBot.EventHandlers.Commands;
using CoreBot.Structures.Command;
using CoreBot.Structures.DataStorage;
using DSharpPlus;
using DSharpPlus.Entities;
using Omnibot.EventHandlers;

namespace BotCore
{
    public class Program
    {
        [MTAThread]
        public static void Main()
        {
            new Program().RunBotAsync().GetAwaiter().GetResult();
        }
        public async Task RunBotAsync()
        {
            Bot kinoBot = new Bot("KinoBot");
            kinoBot.Client.MessageReactionAdded += async (_, e) =>
                {
                    if (e.Channel.Id == 325845026892546049)
                    {
                        Console.WriteLine($"{e.User} has accepted the rules");
                        //DiscordMessage Message = await e.Channel.GetMessageAsync(723915085965426761);
                        await e.Guild.Members[e.User.Id].GrantRoleAsync(e.Guild.Roles[723952097669808128], "Accepted rules");
                    }
                };
            /*kinoBot.Client.GuildAvailable += async (sender, e) =>
            {
                new System.Threading.Thread(async () =>
                {
                    if (e.Guild.Id == 323294629413781514)
                    {
                        foreach (DiscordUser user in await (await e.Guild.Channels[325845026892546049].GetMessageAsync(723915085965426761)).GetReactionsAsync(DiscordEmoji.FromName(sender, ":white_check_mark:"), limit: 100))
                        {
                            try
                            {
                                DiscordMember m = await e.Guild.GetMemberAsync(user.Id);
                                if (!m.Roles.Any(r => r.Id == 723952097669808128))
                                {
                                    await m.GrantRoleAsync(e.Guild.Roles[723952097669808128], "Accepted rules");
                                    Console.WriteLine($"{m} has accepted the rules");
                                }
                            }
                            catch
                            {
                                // ignored
                            }
                        }
                    }
                }).Start();
            };*/
            Bot siliconBotPublic = new Bot("Silicon Bot Public");
            siliconBotPublic.CommandHandler.RegisterCommand("invite", new CommandDefinition(CommandCategory.BuiltIn, "Invite me to your server!")
            {
                Action = async (ce) =>
                {
                    await ce.SendMessageAsync("<https://discordapp.com/oauth2/authorize?client_id=664487904944324627&permissions=-1&scope=bot>");
                }
            }); 
            LegacyServer siliconHeaven = (await siliconBotPublic.DataStore.GetServer(391478785754791949));
            siliconHeaven.Prefix = "\\";
            siliconHeaven.UnknownCommandMessage = false;
            siliconHeaven.Save();
            Bot anonBot = new Bot("AnonBot");
            anonBot.Client.MessageReactionAdded += async (_, e) =>
            {
                if (e.Channel.Id == 718453038460960790)
                {
                    DiscordMessage message = await e.Channel.GetMessageAsync(e.Message.Id);
                    String[] lines = message.Content.Split('\n');
                    foreach (string line in lines)
                    {
                        //string emote = line.Split(' ')[0];
                        string rolename = line.Split(' ', 2)[1];
                        foreach (KeyValuePair<ulong, DiscordRole> role in e.Guild.Roles.ToList())
                        {
                            if (role.Value.Name == rolename)
                            {
                                Console.WriteLine("Found role: " + rolename);
                                await e.Guild.Members[e.User.Id].GrantRoleAsync(e.Guild.GetRole(role.Key), "Self assigned role");
                                break;
                            }
                        }
                    }
                }
            };
            anonBot.Client.MessageReactionRemoved += async (_, e) =>
            {
                if (e.Channel.Id == 718453038460960790)
                {
                    DiscordMessage message = await e.Channel.GetMessageAsync(e.Message.Id);
                    String[] lines = message.Content.Split('\n');
                    foreach (string line in lines)
                    {
                        //string emote = line.Split(' ')[0];
                        string rolename = line.Split(' ', 2)[1];
                        foreach (KeyValuePair<ulong, DiscordRole> role in e.Guild.Roles.ToList())
                        {
                            if (role.Value.Name == rolename)
                            {
                                Console.WriteLine("Found role: " + rolename);
                                await e.Guild.Members[e.User.Id].RevokeRoleAsync(e.Guild.GetRole(role.Key), "Self assigned role");
                                break;
                            }
                        }

                    }
                }
            };
            anonBot.Start();
            kinoBot.Start();
            siliconBotPublic.Start();
            new Bot("HericanBot").Start();
            new Bot("ImpulsBot").Start();
            new Bot("StudioBot").Start();
            new Bot("TheArcaneBot").Start();

            Bot ImpulsYeeter = new Bot("ImpulsYeeter");
            ImpulsYeeterBehaviors iyb = new ImpulsYeeterBehaviors(ImpulsYeeter);
            iyb.RegisterCommands();
            ImpulsYeeter.Start();
            await Task.Delay(-1);
        }
    }
}
