﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Linq;
using System.Threading;

namespace CoreBot
{
    public class Timing
    {
        public bool PrintTimings;
        public Stopwatch StartTiming(string name)
        {
            Stopwatch sw = Timings.GetOrAdd(name, (_) => { Stopwatch lsw = new Stopwatch(); lsw.Start(); return lsw; });
            sw.Reset();
            sw.Start();
            if (PrintTimings)
            {
                Console.WriteLine($"Started timer {name}.");
            }

            new Thread(() =>
            {
                if (Timings.TryGetValue(name, out Stopwatch timing))
                {
                    while (timing.IsRunning)
                    {
                        Extensions.SetStatus($"Currently timing: {name} @ {timing.ElapsedMilliseconds}");
                        if (timing.ElapsedMilliseconds >= 60000)
                        {
                            Fail(name);
                        }

                        Thread.Sleep(1);
                    }
                }
                else
                {
                    Console.WriteLine("Could not find timing with name " + name);
                }
            }).Start();
            return sw;
        }
        public Stopwatch StopTiming(string name)
        {
            if (!Timings.TryGetValue(name, out Stopwatch sw))
            {
                throw new Exception("Timing does not exist!");
            }

            sw.Stop();
            if (PrintTimings)
            {
                Console.WriteLine($"Stopped timer {name}: {sw.ElapsedMilliseconds} ms.");
            }

            Extensions.SetStatus($"Stopped timer {name}: {sw.ElapsedMilliseconds} ms.");
            return sw;
        }
        public Stopwatch GotoNext(Stopwatch old, string name, bool muted = false)
        {
            old.Stop();
            if (!muted && PrintTimings)
            {
                Console.WriteLine($"Stopped timer {Timings.FirstOrDefault(x => x.Value == old).Key}: {old.ElapsedMilliseconds} ms, moving on to {name}.");
            }

            Stopwatch sw = Timings.GetOrAdd(name, (_) => { Stopwatch lsw = new Stopwatch(); lsw.Start(); return lsw; });
            sw.Reset();
            sw.Start();
            return sw;
        }
        public Stopwatch StopTiming(Stopwatch old)
        {
            old.Stop();
            if (PrintTimings)
            {
                Console.WriteLine($"Stopped timer {Timings.FirstOrDefault(x => x.Value == old).Key}: {old.ElapsedMilliseconds} ms.");
            }

            return old;
        }
        public Stopwatch Fail(string name)
        {
            if (!Timings.TryGetValue(name, out Stopwatch sw))
            {
                throw new Exception("Timing does not exist!");
            }

            sw.Stop();
            //Console.WriteLine($"Failed timer {Name} after {sw.ElapsedMilliseconds} ms.");
            Extensions.SetStatus($"Failed timer {name} after {sw.ElapsedMilliseconds} ms.");
            //Log.SbLog($"Failed timer {Name} after {sw.ElapsedMilliseconds} ms.");
            return sw;
        }

        public ConcurrentDictionary<string, Stopwatch> Timings = new ConcurrentDictionary<string, Stopwatch>();
    }
}
