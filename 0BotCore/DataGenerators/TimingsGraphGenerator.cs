﻿using BotCore.Structures.Base;

using CoreBot;

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace BotCore.DataGenerators
{
    public class TimingsGraphGenerator
    {
        Bot bot;
        Timing timing;
        public TimingsGraphGenerator(Bot bot)
        {
            this.bot = bot;
            timing = bot.Timing;
        }
        //bool detailed = false;

        //used during processing
        private int i;//, ti = 0;
        private readonly int GraphDimensions = 512;

        public void Generate(Stream stream = null, bool allBots = false)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            i = 0;
            using (Bitmap bmp = new Bitmap(GraphDimensions + 500, GraphDimensions))
            {
                using (Graphics g = Graphics.FromImage(bmp))
                {
                    DrawGraph(g, allBots: allBots);
                }
                if (stream != null)
                {
                    using MemoryStream bstream = new MemoryStream();
                    bmp.Save(bstream, System.Drawing.Imaging.ImageFormat.Png);
                    stream.Write(bstream.ToArray(), 0, bstream.ToArray().Length);
                    //bmp.Save(stream, ImageFormat.Png);
                }
                else
                {
                    try { bmp.Save("test.png"); File.Copy("test.png", "F:\\nginx\\html\\timing.png", true); }
                    catch
                    {
                        // ignored
                    }
                }
            }

            sw.Stop();
            bot.Timing.Timings.AddOrUpdate(allBots ? "Generating all graphs" : "Generating graph", _ => sw, (_, _) => sw);
            //Console.WriteLine((double)sw.ElapsedTicks/(TimeSpan.TicksPerSecond/1000));
        }

        private int graphSize, xo, yo;
        //private Graphics graphics;
        //private SolidBrush brush;
        private ColorGenerator colorGenerator = new ColorGenerator();

        private void Setup(int size = -1)
        {
            if (size < 0)
            {
                size = GraphDimensions;
            }

            graphSize = size;
            colorGenerator = new ColorGenerator();
        }
        private void DrawGraph(Graphics g, int size = -1, bool allBots = false)
        {
            Setup(size);
            float lastAngle = -90;
            Dictionary<string, double> timings = GetTimings();
            if (allBots) {
                timings.Clear();
                foreach (Bot ibot in BotManager.Bots.Values)
                {
                    foreach (KeyValuePair<string, double> item 
                            in ibot.TimingsGraphGenerator.GetTimings())
                    {
                        timings.Add($"[{ibot.Name}] {item.Key}", item.Value);
                    }
                }

                timings = timings.OrderByDescending(key => key.Value).ToDictionary(t => t.Key, t => t.Value);
            }
            double sum = timings.Sum(x => x.Value);
            if (timings.ContainsKey("Generating graph"))
            {
                sum -= timings["Generating graph"];
            }
            if (timings.ContainsKey("Generating all graphs"))
            {
                sum -= timings["Generating all graphs"];
            }

            if (timings.ContainsKey("Connecting to Discord"))
            {
                sum -= timings["Connecting to Discord"];
            }

            if (timings.ContainsKey("Message"))
            {
                sum -= timings["Message"];
            }

            DrawString(g, $"Timing entries for {bot.Name} ({(timings.ContainsKey("Generating graph") ? Math.Round(timings["Generating graph"], 1) : 0)}ms, {(timings.ContainsKey("Message") ? Math.Round(timings["Message"], 1) : 0)}ms spent processing messages)");
            foreach (KeyValuePair<string, double> entry in timings)
            {
                if (!(entry.Key == "Connecting to Discord" || entry.Key == "Generating graph" || entry.Key == "Generating all graphs" || entry.Key == "Message"))
                {
                    Brush brush = new SolidBrush(ColorTranslator.FromHtml("#" + colorGenerator.NextColour()));
                    g.FillPie(brush, new Rectangle(0, 0, graphSize, graphSize), lastAngle, (float)(entry.Value / sum) * 360);
                    lastAngle += (float)(entry.Value / sum) * 360;
                    DrawLegendEntry(g, $"{entry.Key}: {Math.Round(entry.Value, 1)}ms", brush);
                }
            }

        }
        private void DrawString(Graphics graphics, string text, bool isLegendEntry = false)
        {
            (xo, yo) = GetPos();
            if (isLegendEntry)
            {
                xo += 15;
            }
            else
            {
                i++;
            }

            if (BotManager.IsSingleBot) text = text.Split("] ").Last();
            graphics.DrawString(text, SystemFonts.DefaultFont, new SolidBrush(Color.White), new PointF(xo, yo));
        }
        private void DrawLegendEntry(Graphics graphics, string text, Brush brush)
        {
            (xo, yo) = GetPos();
            graphics.FillRectangle(brush, xo, yo + 2, 10, 10);
            DrawString(graphics, text, true);
            i++;
        }
        private (int x, int y) GetPos()
        {
            /*if (false)
            {
                x = 500 * (i % 2);
                y = GraphDimensions + i / 2 * 16;
            }
            else */
            var y = i * 16;
            return (graphSize + 10, y);
        }

        private Dictionary<string, double> GetTimings()
        {
            ConcurrentDictionary<string, double> timings = new ConcurrentDictionary<string, double>();
            while (!Parallel.ForEach(timing.Timings, (tim) =>
            {
                {
                    timings.AddOrUpdate(
                        Regex.Replace(tim.Key, @"[\d-]", string.Empty).Replace("__", "_"),
                        tim.Value.Elapsed.Ticks / (double)TimeSpan.TicksPerMillisecond,
                        (_, old) => old + tim.Value.Elapsed.Ticks / (double)TimeSpan.TicksPerMillisecond
                    );
                }
            }).IsCompleted)
            {
                Thread.Sleep(0);
            }

            return timings.OrderByDescending(key => key.Value).ToDictionary(t=>t.Key, t=>t.Value);
        }
    }
}
