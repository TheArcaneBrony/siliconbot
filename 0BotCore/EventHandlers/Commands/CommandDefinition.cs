﻿using CoreBot.EventHandlers.Commands;

using Newtonsoft.Json;

using System;

namespace CoreBot.Structures.Command
{
    public static class DefaultCommandImplementation
    {
        public static Action<CommandEnvironment> DefaultAction = async (ce) => { await ce.SendMessageAsync("This command has not been implemented yet!"); };
    }

    public class CommandDefinition
    {
        [Obsolete("Please use the new constructor!")]
        public CommandDefinition() { }

        // public CommandDefinition (CommandCategory _category, string _description) {
        //     Category = _category;
        //     Description = _description;
        // }
        // public CommandDefinition (CommandCategory _category, string _description, Action<CommandEnvironment> _action) {
        //     Category = _category;
        //     Description = _description;
        //     Action = _action;
        // }
        public CommandDefinition(CommandCategory category = CommandCategory.Hidden, string description = "", Action<CommandEnvironment> action = null, bool donator = false)
        {
            Category = category;
            Description = description;
            Action = action;
            Donator = donator;
            if (action == null)
            {
                Action = async (ce) => { await ce.SendMessageAsync("Command code not defined! Please report!"); };
            }
        }
        public string Name;
        public string Description = "";
        public bool Donator;
        public CommandCategory Category = CommandCategory.None;
        [JsonIgnore]
        public Action<CommandEnvironment> Action = async (ce) => { await ce.SendMessageAsync("Command code not defined! Please report!"); };
        public bool HasPermission(CommandEnvironment ce)
        {
            if (Checks.IsBotOwner(ce))
            {
                return true;
            }
            else if (Category == CommandCategory.Owner)
            {
                return false;
            }
            else if (Category == CommandCategory.Developer)
            {
                return Checks.IsBotDeveloper(ce);
            }
            else if (Category == CommandCategory.Moderation)
            {
                return Checks.IsStaff(ce);
            }
            /*else if (Donator) return ce.Author.Donator;*/
            else
            {
                return true;
            }
        }
    }

    public enum CommandCategory
    {
        None,
        Hidden,
        BuiltIn,
        Base,
        Discord,
        Economy,
        Fun,
        Leveling,
        Games,
        Hardware,
        Ticket,
        Music,
        Minecraft,
        Moderation,
        Developer,
        Owner
    }
}
