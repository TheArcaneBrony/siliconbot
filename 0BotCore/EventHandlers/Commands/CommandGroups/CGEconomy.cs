﻿using BotCore.Structures.Base;

using CoreBot.Structures.Command;

using System;
using System.Collections.Generic;

namespace CoreBot.EventHandlers.Commands
{
    internal class CgEconomy
    {

        public static void SetupCommands(Bot bot)
        {
            if (bot.Name != "Omnibot") return;
            bot.CommandHandler.RegisterCommand("daily",
            new CommandDefinition(CommandCategory.Economy, "Recieve your daily credits")
            {
                Action = async (ce) =>
                {
                    //code
                    if (DateTime.Now.Subtract(ce.Author.LastCreditsRedeem).TotalDays >= 1)
                    {
                        ulong addCreds = (ulong)RuntimeInfo.Rnd.Next(1, 20);
                        if (ce.Author.IsPremium)
                        {
                            addCreds = (ulong)(addCreds * 1.5);
                        }

                        ce.Author.Credits += addCreds;
                        ce.Author.LastCreditsRedeem = DateTime.Now;
                        await ce.SendMessageAsync($"You received {addCreds} credits!{(ce.Author.IsPremium ? "" : " You can get 50% more credits daily by going premium! See <<premium for information!")}");
                    }
                    else
                    {
                        TimeSpan waitTime = DateTime.Now.Subtract(ce.Author.LastCreditsRedeem);
                        await ce.SendMessageAsync($"You need to wait {23 - waitTime.Hours} hours, {59 - waitTime.Minutes} minutes and {59 - waitTime.Seconds} seconds");
                    }
                }
            }
        );
            bot.CommandHandler.RegisterCommand("slots",
                new CommandDefinition(CommandCategory.Economy, "Let's see if you win")
                {
                    Action = async (ce) =>
                    {
                        if (ce.Args.Count == 1 && ulong.TryParse(ce.Args[0], out ulong inCredits) && ce.Author.Credits >= inCredits)
                        {
                            ce.Author.Credits -= inCredits;
                            string[] possibleSlots = new[] { ":unicorn:", ":gun:", ":horse:", ":cherries:", ":cherry_blossom:" };
                            List<string> slots = new List<string>();
                            string slotString = "--------------------\n";
                            double multiplier = 0;
                            for (int i = 0; i < 3; i++)
                            {
                                slots.Add(possibleSlots[RuntimeInfo.Rnd.Next(0, possibleSlots.Length)]);
                                slotString += $"| {slots[i]} ";
                            }
                            slotString += "|\n--------------------";
                            if (slots[0] == slots[1] || slots[1] == slots[2] || slots[0] == slots[2])
                            {
                                multiplier = 1.25;
                            }
                            if (slots[0] == slots[1] && slots[1] == slots[2])
                            {
                                multiplier = 2;
                            }
                            ulong outCredits = (ulong)(inCredits * multiplier);
                            ce.Author.Credits += outCredits;
                            string state = outCredits <= 0 ? $"lost {inCredits}" : $"won {outCredits}";
                            await ce.SendMessageAsync($"{slotString}\nYou have {state} credits and now have {ce.Author.Credits} credits!");
                        }
                        else
                        {
                            await ce.SendMessageAsync("You entered an amount of credits that you don't have!");
                        }
                    }
                }
            );
            bot.CommandHandler.RegisterCommand("bal",
                new CommandDefinition(CommandCategory.Economy, "Display your current balance")
                {
                    Action = async (ce) =>
                    {
                        if (ce.Args.Count == 1)
                        {
                            await ce.SendMessageAsync($"**{ce.Pinged.Username}** has {ce.Pinged.Credits} credits.");
                        }
                        else
                        {
                            await ce.SendMessageAsync($"**{ce.E.Author.Username}** has {ce.Author.Credits} credits.");
                        }
                    }
                }
            );
        }
    }
}
