﻿using System.Diagnostics;
using BotCore.Structures.Base;
using CoreBot.Structures.DataStorage;
using DSharpPlus.Entities;

namespace CoreBot.EventHandlers.MessageHandlerModules
{
    public class MessageInteractions
    {
        private Bot bot;

        public MessageInteractions(Bot bot)
        {
            this.bot = bot;
        }

        public async void CheckMessage(string timingId, LegacyServer server, LegacyUser author, DiscordMessage msg)
        {
            Stopwatch st = bot.Timing.StartTiming(timingId + ".Checks");
            if (msg.Content.ToLower().StartsWith("poll: "))
            {
                await msg.CreateReactionAsync(DiscordEmoji.FromName(bot.Client, ":white_check_mark:"));
                await msg.CreateReactionAsync(DiscordEmoji.FromName(bot.Client, ":negative_squared_cross_mark:"));
                await msg.CreateReactionAsync(DiscordEmoji.FromName(bot.Client, ":grey_question:"));
            }

            bot.Timing.StopTiming(st);
        }
    }
}