﻿using CoreBot.Structures.DataStorage;

using DSharpPlus;
using DSharpPlus.EventArgs;

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;

namespace CoreBot.EventHandlers.Commands
{
    public class CommandEnvironment
    {
        /*public CommandEnvironment(MessageCreateEventArgs EventArgs)
        {
            e = EventArgs;
            Server = bot.dataStore.GetServer(e.Guild.Id);
            Author = Server.GetUser(e.Author.Id, false);
            Pinged = e.MentionedUsers.Count >= 1 ? Server.GetUser(e.MentionedUsers[0].Id) : Author;
            Command = e.Message.Content.Split(' ')[0].Remove(0, Server.Prefix.Length).Replace("@everyone", "@ everyone").Replace("@here", "@ here");
            Message = e.Message.Content.Replace("@everyone", "@ everyone").Replace("@here", "@ here").Remove(0, Server.Prefix.Length + Command.Length);
            Args = Message.Split(' ').ToList();
            Args.RemoveAt(0);
        }*/
        public CommandEnvironment(DiscordClient client, MessageCreateEventArgs eventArgs, LegacyServer server, LegacyUser author, LegacyUser pinged)
        {
            Client = client;
            E = eventArgs;
            Server = server;
            Author = author;
            Pinged = pinged;
            Command = E.Message.Content.Split(' ')[0].Remove(0, Server.Prefix.Length).Replace("@everyone", "@ everyone").Replace("@here", "@ here");
            Message = E.Message.Content.Replace("@everyone", "@ everyone").Replace("@here", "@ here").Remove(0, server.Prefix.Length + Command.Length);
            Args = Message.Split(' ').ToList();
            Args.RemoveAt(0);
        }
        public DiscordClient Client;
        public MessageCreateEventArgs E;
        public LegacyServer Server;
        public LegacyUser Author;
        public LegacyUser Pinged;
        public string Command;
        public string Message;
        public List<string> Args;
        public DiscordChannel Channel => E.Channel;
        public DiscordMember Member => E.Channel.Users.ToArray()[E.Author.Id];

        public async Task<DiscordMessage> SendMessageAsync(string content = "", DiscordEmbed embed = null)
        {
            try
            {
                return await E.Channel.SendMessageAsync(content, embed: embed);
            }
            catch (UnauthorizedException)
            {
                if(Server.ShouldDMIfPermissionDenied)
                    try
                    {
                        return await Member.SendMessageAsync(content, embed: embed);
                    }
                    catch (UnauthorizedException)
                    {
                    
                    }
            }

            return null;
        }
        public async Task<DiscordMessage> SendFileAsync(string filename, string content = "", DiscordEmbed embed = null)
        {
            try
            {
                return await E.Channel.SendFileAsync(filename, content, embed: embed);
            }
            catch (UnauthorizedException)
            {
                if(Server.ShouldDMIfPermissionDenied)
                    try
                    {
                        return await Member.SendFileAsync(filename, content, embed: embed);
                    }
                    catch (UnauthorizedException)
                    {
                    
                    }
            }

            return null;
        }
    }
}
