﻿using Newtonsoft.Json;

namespace Omnibot.Structures.LegacyDataStorage
{
    
    [JsonObject(MemberSerialization.OptOut)]
    public class Ticket
    {
        public ulong OwnerId;
        public ulong ChannelId;
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Include)]
        public bool IsOpen = true;
    }
}