﻿using System.Collections.Generic;

namespace SiliconBotCore3
{
    public partial class DServerUser
    {
        public DServerUser()
        {
            Quote = new HashSet<Quote>();
            WarningAuthor = new HashSet<Warning>();
            WarningUser = new HashSet<Warning>();
        }

        public long UserId { get; set; }
        public long DUserId { get; set; }
        public long DServerId { get; set; }

        public virtual Server DServer { get; set; }
        public virtual ICollection<Quote> Quote { get; set; }
        public virtual ICollection<Warning> WarningAuthor { get; set; }
        public virtual ICollection<Warning> WarningUser { get; set; }
    }
}
