﻿using System.Collections.Generic;
// ReSharper disable All

namespace SiliconBotCore3
{
    public partial class Server
    {
        public Server()
        {
            DServerUser = new HashSet<DServerUser>();
        }

        public long ServerId { get; set; }
        public long DServerId { get; set; }

        public virtual ICollection<DServerUser> DServerUser { get; set; }
    }
}
