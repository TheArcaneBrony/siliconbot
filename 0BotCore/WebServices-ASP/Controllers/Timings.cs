﻿using BotCore.Structures.Base;
using BotCore.WebServices;

using Microsoft.AspNetCore.Mvc;

using System.IO;
using BotCore.DataGenerators;

namespace BotCore.WebServices_ASP.Controllers
{
    [Controller]
    [Route("/timings")]
    public class TimingsController : ControllerBase
    {
        [HttpGet]
        public ContentResult Get()
        {
            return new ContentResult { Content = NavbarProvider.GetNavBar() + @"<head>
    <link rel=stylesheet href='/timings.css'>
    <script type='text/JavaScript'>
      var timeoutPeriod = 500;
      var x=0, y=0;
      var img = new Image();
      var img2 = new Image();
      img.onload = function() {
          setTimeout(timedRefresh,timeoutPeriod);
          var canvas = document.getElementById('graph');
          var context = canvas.getContext('2d');
          context.clearRect(0, 512, canvas.width, canvas.height-512);
          context.clearRect(512, 0, canvas.width-512, canvas.height);
          context.drawImage(img, 0,0);
      };
      img2.onload = function() {
          setTimeout(timedRefresh,timeoutPeriod);
          var canvas = document.getElementById('graph2');
          var context = canvas.getContext('2d');
          context.clearRect(0, 512, canvas.width, canvas.height-512);
          context.clearRect(512, 0, canvas.width-512, canvas.height);
          context.drawImage(img2, 0,0);
      };

      img.onerror = img2.onerror = function() {
          setTimeout(timedRefresh,timeoutPeriod);
      };

      function timedRefresh() {
          img.src = 'timings.png?d=' + Date.now();
          img2.src = 'timings-all.png?d=' + Date.now();
      }
      timedRefresh();
      var moused = false;
    </script>
    <title>Bot timings</title>
</head>

<body>
    <canvas id='graph' width='1280' height='512'></canvas>
    <canvas id='graph2' width='1280' height='512'></canvas>
    <div id='controls'>
        <div style='left: auto; right: auto; text-align: center;'>
            <a>Reload time: </a><a id='ttimeout'>500</a><a> ms</a><br />
            <input id='timeout' type='range' min='10' max='1000' value='500' />
        </div>
    </div>
    <script>
        document.onmousedown = function () {
            moused = true
        }
        document.onmouseup = function () {
            moused = false
        }
        document.getElementById('timeout').onmousemove = function () {
            if (moused) {
                document.getElementById('ttimeout').innerText = timeoutPeriod = document.getElementById('timeout')
                    .value;
            }
        }
    </script>
</body>
</html>", ContentType = "text/html" };
        }
    }
    [Controller]
    [Route("/timings.png")]
    public class TimingsImgController : ControllerBase
    {
        public TimingsImgController(Bot bot)
        {
            this.bot = bot;
        }
        private Bot bot;
        [HttpGet]
        public FileResult Get()
        {
            MemoryStream stream = new MemoryStream();
            bot.TimingsGraphGenerator.Generate(stream);
            stream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(stream, "image/png");
        }
    }
    [Controller]
    [Route("/timings-all.png")]
    public class TimingsAllImgController : ControllerBase
    {
        public TimingsAllImgController(Bot bot)
        {
            this.bot = bot;
        }
        private Bot bot;
        [HttpGet]
        public FileResult Get()
        {
            MemoryStream stream = new MemoryStream();
            new TimingsGraphGenerator(bot).Generate(stream, true);
            stream.Seek(0, SeekOrigin.Begin);
            return new FileStreamResult(stream, "image/png");
        }
    }
}
